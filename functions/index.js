const functions = require("firebase-functions");
const cors = require("cors")({ origin: true });
const admin = require("firebase-admin");
const cert = require("./newzhype-f4575-firebase-adminsdk-78yw4-50c0a38acf.json");
const { database } = require("firebase-admin");

admin.initializeApp(functions.config().firebase);

exports.notifyOnReactions = functions.database
  .ref("/posts/{postId}/reactions/{reactorId}/")
  .onCreate(async (snapshot, context) => {
    const postId = context.params.postId;
    const reactorId = context.params.reactorId;

    console.log("POST_RRR_ID", reactorId);

    try {
      admin
        .database()
        .ref("posts")
        .child(postId)
        .once("value")
        .then(function (postSnap) {
          if (postSnap.exists()) {
            let ownerUid = postSnap.val().ownerId;
            if (ownerUid) {
              console.log("POSTER--", ownerUid);
              const ownerDbRef = admin.database().ref("users").child(ownerUid);
              ownerDbRef.once("value", async function (dataSnapshot) {
                console.log("POSTER--USEROBJ", dataSnapshot);

                if (dataSnapshot.exists()) {
                  console.log("POSTER--USEROBJ2", dataSnapshot.val());

                  const receiverToken = dataSnapshot.val().fcmToken;

                  if (receiverToken) {
                    console.log("POSTER--TOKEN", receiverToken);

                    const payload = {
                      notification: {
                        title: "TunedOut",
                        body: "Somebody has reacted to your post.",
                      },
                      data: {
                        postType: postSnap.val().postCategory,
                        postId: postSnap.val().postId,
                      },
                    };
                    await sendPushNotification(receiverToken, payload);
                  }
                }
              });
            }
          }
        });
    } catch (err) {
      console.error(err);
    }
  });

async function sendPushNotification(userToken, payload) {
  try {
    admin.messaging().sendToDevice(userToken, payload);
  } catch (ex) {
    console.error(ex);
  }
}

exports.notifyOnComments = functions.database
  .ref("/posts/{postId}/comments/{reactorId}/")
  .onCreate(async (snapshot, context) => {
    const postId = context.params.postId;
    const reactorId = context.params.reactorId;

    console.log("POST_RRR_ID", reactorId);

    try {
      admin
        .database()
        .ref("posts")
        .child(postId)
        .once("value")
        .then(function (postSnap) {
          if (postSnap.exists()) {
            let ownerUid = postSnap.val().ownerId;
            if (ownerUid) {
              console.log("POSTER--", ownerUid);
              const ownerDbRef = admin.database().ref("users").child(ownerUid);
              ownerDbRef.once("value", async function (dataSnapshot) {
                console.log("POSTER--USEROBJ", dataSnapshot);

                if (dataSnapshot.exists()) {
                  console.log("POSTER--USEROBJ2", dataSnapshot.val());

                  const receiverToken = dataSnapshot.val().fcmToken;

                  if (receiverToken) {
                    console.log("POSTER--TOKEN", receiverToken);

                    const payload = {
                      notification: {
                        title: "TunedOut",
                        body: "Somebody has commented on your post.",
                      },
                      data: {
                        postType: postSnap.val().postCategory,
                        postId: postSnap.val().postId,
                      },
                    };
                    await sendPushNotification(receiverToken, payload);
                  }
                }
              });
            }
          }
        });
    } catch (err) {
      console.error(err);
    }
  });

exports.notifyReplies = functions.database
  .ref("/posts/{postId}/comments/{commentId}/replies/{replyId}")
  .onCreate(async (snapshot, context) => {
    const POST_ID = context.params.postId;
    const COMM_ID = context.params.commentId;
    // const commentId = context.params.commentId;

    console.error("THIS_REPLIED", snapshot.val().commenterId);
    console.error("POST_ID", POST_ID);

    try {
      admin
        .database()
        .ref("posts")
        .child(POST_ID)
        .child("comments")
        .child(COMM_ID)
        .once("value")
        .then(function (postSnap) {
          const idToNotify = postSnap.val().commenterId;
          if (idToNotify) {
            console.error("COMMENTOWNER--", idToNotify);
            const ownerDbRef = admin.database().ref("users").child(idToNotify);
            ownerDbRef.once("value", async function (dataSnapshot) {
              if (dataSnapshot.exists()) {
                console.error("POSTER--USEROBJ2", dataSnapshot.val());
                const receiverToken = dataSnapshot.val().fcmToken;
                console.error("POSTER--TOKEN", dataSnapshot.val().fcmToken);
                if (receiverToken) {
                  console.error("POSTER--TOKEN", receiverToken);

                  const payload = {
                    notification: {
                      title: "TunedOut",
                      body: "Somebody has replied to your comment.",
                    },
                    data: {
                      postId: POST_ID,
                    },
                  };
                  await sendPushNotification(receiverToken, payload);
                }
              }
            });
          }
        });
      // admin.database().ref('posts').child(postId).once("value").then(function (postSnap) {

      // });
    } catch (err) {
      console.error(err);
    }
  });

exports.notifyCommentsOnVideo = functions.firestore
  .document("videoRendering/{videoId}/comments/{commentId}")
  .onCreate(async (snapshot, context) => {
    const videoId = context.params.videoId;
    const data = snapshot.data();
    try {
      const videoData = (
        await admin.firestore().collection("videoRendering").doc(videoId).get()
      ).data();

      const owner = (await videoData.user.get()).data();
      console.log("user", owner, videoData);
      const user = (await data.user.get()).data();

      const receiverToken = owner.fcmToken;

      if (receiverToken) {
        console.log("POSTER--TOKEN", receiverToken);

        const payload = {
          notification: {
            title: "TunedOut",
            body: user.firstName + " has commented to your Video.",
          },
          data: {
            postType: "video",
            postId: videoId,
          },
        };
        await sendPushNotification(receiverToken, payload);
      }
      console.log("done ");
    } catch (error) {
      console.log(error, "error ");
    }
  });
exports.notifyReactionsOnVideo = functions.firestore
  .document("videoRendering/{videoId}")
  .onWrite(async (change, context) => {
    const videoId = context.params.videoId;
    const prevData = change.before.data();
    const newData = change.after.data();

    if (prevData.like.length < newData.like.length)
      try {
        const owner = (await newData.user.get()).data();

        const receiverToken = owner.fcmToken;

        if (receiverToken) {
          console.log("POSTER--TOKEN", receiverToken);

          const payload = {
            notification: {
              title: "TunedOut",
              body: "Somebody has reacted to your Video.",
            },
            data: {
              postType: "video",
              postId: videoId,
            },
          };
          await sendPushNotification(receiverToken, payload);
        }
        console.log("done ");
      } catch (error) {
        console.log(error, "error ");
      }
  });

exports.notifyOnMessage = functions.firestore
  .document("messages/{groupId}/chats/{chatId}")
  .onCreate(async (snapshot, context) => {
    const {chatId , groupId} = context.params;

    const data = snapshot.data();
    const { sender: sId, reciver: rId } = data;
    
    try {
      const sender = (
        await admin.firestore().collection("users").doc(sId).get()
      ).data();
      const reciver = (
        await admin.firestore().collection("users").doc(rId).get()
      ).data();

      const receiverToken = reciver.fcmToken;

      if (receiverToken) {
        console.log("POSTER--TOKEN", receiverToken);

        const payload = {
          notification: {
            title: "TunedOut",
            body:  "You have a new message from "+ sender.firstName+ ".",
          },
          data: {
            postType: "message",
            chatId , groupId
          },
        };
        await sendPushNotification(receiverToken, payload);
      }
      console.log("done ");
    } catch (error) {
      console.log(error, "error ");
    }
  });
